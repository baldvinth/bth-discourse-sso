﻿using BTh.Discourse.Sso.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace BTh.Discourse.Sso
{
    /// <summary>
    /// Contents of the payload we can expect from
    /// Discourse SSO
    /// </summary>
    public class DiscoursePayload
    {
        public string Nonce { get; set; }
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Validate signature, then opens the sso payload
        /// </summary>
        /// <param name="sso"></param>
        /// <param name="sig"></param>
        /// <param name="secret"></param>
        public DiscoursePayload(string sso, string sig, string secret)
        {
            // Validate the signature for the payload
            if (!Crypto.IsSignatureValid(secret, sso, sig))
                throw new ArgumentException("Signature for this payload is invalid");

            OpenPayload(sso);
        }

        /// <summary>
        /// Less secure: Skips validation and opens the sso payload
        /// You should choose to validate the payload signature when possible, unless you have already done it in another step
        /// </summary>
        /// <param name="sso"></param>
        public DiscoursePayload(string sso)
        {
            OpenPayload(sso);
        }

        /// <summary>
        /// Decrypt the payload and add values to Nonce and ReturnUrl
        /// </summary>
        /// <param name="sso"></param>
        private void OpenPayload(string sso)
        {
            // Decode the payload
            string payload = Crypto.ConvertBase64ToString(sso);

            // Split the string into a list
            List<string> values = payload.Split('&').ToList();

            // Retrieve the nonce and return url
            Nonce = values.FirstOrDefault(x => x.StartsWith("nonce")).Split('=')[1];
            ReturnUrl = WebUtility.UrlDecode(values.FirstOrDefault(x => x.StartsWith("return_sso_url")).Split('=')[1]);
        }
    }
}

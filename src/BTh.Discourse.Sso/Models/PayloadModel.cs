﻿using BTh.Discourse.Sso.Utils;
using System;
using System.Collections.Generic;

namespace BTh.Discourse.Sso
{
    /// <summary>
    /// Payload model we send to Discourse
    /// after everything has been verified
    /// </summary>
    public class Payload
    {
        public string Nonce { get; }
        public string Email { get; }
        public string ExternalID { get; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string AvatarUrl { get; set; }
        public string Biography { get; set; }
        public List<string> AddGroups { get; set; }
        public List<string> RemoveGroups { get; set; }

        // More of optional features
        public bool AvatarForceUpdate { get; set; }
        public bool EmailRequireActivation { get; set; }
        public bool SuppressWelcomeMessage { get; set; }

        /// <summary>
        /// A payload for Discourse must always contain
        /// the nonce provided by the forums, an e-mail address
        /// and an external ID
        /// </summary>
        /// <param name="nonce"></param>
        /// <param name="email"></param>
        /// <param name="externalID"></param>
        public Payload(string nonce, string email, string externalID)
        {
            // Validate that none of the mandatory fields is null or empty
            if (string.IsNullOrEmpty(nonce) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(externalID))
                throw new NullReferenceException("Nonce, e-mail address, and external ID cannot be null or an empty string");

            // Set the mandatory fields
            Nonce = nonce;
            Email = email;
            ExternalID = externalID;

            // Initialize lists
            AddGroups = new List<string>();
            RemoveGroups = new List<string>();
        }

        /// <summary>
        /// Generates a Base64 encoded payload
        /// to send to Discourse forums
        /// </summary>
        /// <returns></returns>
        public string GetPayload()
        {
            // Starting the payload out with mandatory fields
            string payload = $"nonce={Nonce}&email={Email}&external_id={ExternalID}";

            // Add username, if provided. Defaults to e-mail username.
            if (!string.IsNullOrEmpty(Username))
                payload += $"&username={Username}";

            // Full name - if enabled. Defaults to e-mail username.
            if (!string.IsNullOrEmpty(FullName))
                payload += $"&name={FullName}";

            // Avatar url - if enabled. Cached resource - can be flushed with AvatarForceUpdate
            if (!string.IsNullOrEmpty(AvatarUrl))
                payload += $"&avatar_url={AvatarUrl}";

            // Flush the avatar cache - needs to be same  URL that was provided originally
            if (!string.IsNullOrEmpty(AvatarUrl) && AvatarForceUpdate)
                payload += $"&avatar_force_update=true";

            // Add biography for Discourse forums
            if (!string.IsNullOrEmpty(Biography))
                payload += $"&bio={Biography}";

            // Groups to add the user to
            if (AddGroups != null && AddGroups.Count > 0)
                payload += $"&add_groups={string.Join(",", AddGroups)}";

            // Groups to remove the user from
            if (RemoveGroups != null && RemoveGroups.Count > 0)
                payload += $"&remove_groups={string.Join(",", RemoveGroups)}";

            // Suppress the welcome message, if the user is signing up
            // for the first time
            if (SuppressWelcomeMessage)
                payload += "&suppress_welcome_message=true";

            // Enable if you have not verified the e-mail address
            if (EmailRequireActivation)
                payload += "&require_activation=true";

            return Crypto.ConvertStringToBase64(payload);
        }

        /// <summary>
        /// Generate the signature for the current payload data
        /// </summary>
        /// <param name="secret"></param>
        /// <returns></returns>
        public string GetSignature(string secret)
        {
            return Crypto.CreateHMACSHA256(secret, GetPayload());
        }
    }
}

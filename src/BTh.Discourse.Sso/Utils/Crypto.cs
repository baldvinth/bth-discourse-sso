﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BTh.Discourse.Sso.Utils
{
    public class Crypto
    {
        /// <summary>
        /// Return whether a signature from Discourse is valid
        /// </summary>
        /// <param name="secret"></param>
        /// <param name="sso"></param>
        /// <param name="sig"></param>
        /// <returns></returns>
        public static bool IsSignatureValid(string secret, string sso, string sig)
        {
            if (string.IsNullOrEmpty(secret) || string.IsNullOrEmpty(sso) || string.IsNullOrEmpty(sig))
                return false;

            return sig == CreateHMACSHA256(secret, sso);
        }

        /// <summary>
        /// Create HMAC-SHA256 encoded string
        /// </summary>
        /// <param name="secret"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string CreateHMACSHA256(string secret, string message)
        {
            var hash = new HMACSHA256(ConvertStringToBytes(secret));
            return ConvertBytesToString(hash.ComputeHash(ConvertStringToBytes(message)));
        }

        /// <summary>
        /// Decode Base64 encoded string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertBase64ToString(string value)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(value));
        }

        /// <summary>
        /// Convert string to Base64 encoded string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertStringToBase64(string value)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
        }

        /// <summary>
        /// Convert a string to byte array
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static byte[] ConvertStringToBytes(string value)
        {
            var encoding = new ASCIIEncoding();
            return encoding.GetBytes(value);
        }

        /// <summary>
        /// Convert an array of bytes to a string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string ConvertBytesToString(byte[] value)
        {
            return BitConverter.ToString(value).Replace("-", string.Empty).ToLower();
        }
    }
}
